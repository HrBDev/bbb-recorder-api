import os
from subprocess import PIPE, Popen

from flask import Flask
from flask_restx import Api, Resource, reqparse
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from xvfbwrapper import Xvfb

app = Flask(__name__)
api = Api(app, version='0.0.1', title='BBB-Recorder API',
          description='BBB-Recorder API',
          )

ns = api.namespace('v1')

# record = api.model('Record', {
#     'url': fields.String(readOnly=True, type=str, description='The task unique identifier'),
#     'sTime': fields.String(required=False, type=str, description='The task details'),
#     'eTime': fields.String(required=True, type=str, description='The task details'),
#     'name': fields.String(required=True, type=str, description='The task details')
# })
#
# info = api.model('Info', {
#     'url': fields.Integer(readOnly=True, description='The task unique identifier'),
# })

parser = reqparse.RequestParser()
parser.add_argument('url', required=False, type=str, help="Url is required!")
parser.add_argument('sTime', required=False, type=str, help="Start Time")
parser.add_argument('eTime', required=False, type=str, help="End Time is required!")
parser.add_argument('name', required=False, type=str, help="Name is required!")


def record(url, name, eTime):
    os.system('node export.js "{0}" {1} {2} true'.format(url, name, eTime))


@ns.route('/record')
class Record(Resource):
    def post(self):
        args = parser.parse_args()

        url = args['url']
        if '&t=' in args['url']:
            url = args['url'].split('&')[0]
        if args['sTime'] is not None:
            url = url + "&t=" + args['sTime'] + "s"

        try:
            Popen('node export.js "{0}" {1} {2} true'.format(url, args['name'], args['eTime']), shell=True, stdout=PIPE,
                  stderr=PIPE)
            # thr = threading.Thread(target=record, args=(url, args['name'], args['eTime']))
            # thr.start()
            return {'message': 'Recording started!', 'url': url}
        except Exception as e:
            return {'message': str(e)}, 500


# Fake header to circumvent blocking script
headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Encoding': 'gzip, deflate',
    'Accept-Language': 'en-GB,en-US;q=0.9,en;q=0.8',
    'Dnt': '1',
    'Upgrade-Insecure-Requests': '1',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) '
                  'AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/83.0.4103.97 Safari/537.36',
    'X-Amzn-Trace-Id': 'Root=1-5ee7bbec-779382315873aa33227a5df6'}


@ns.route('/getVideoInfo')
class Video(Resource):
    def post(self):
        args = parser.parse_args()
        with Xvfb() as xvfb:
            driver = webdriver.Chrome()
            url = args['url']
            print(url)
            if '&t=' in args['url']:
                url = url.split('&')[0]
            driver.get(url + '&t=99h')
            print('Opened url')
            try:
                # myElem = WebDriverWait(driver, 60).until(EC.title_contains((By.XPATH, '/html/body/div[4]/div/div[2]/div[2]/div[1]/div[1]/div[2]/span')))
                myElem = WebDriverWait(driver, 60).until(lambda x: x.find_element_by_xpath(
                    '/html/body/div[4]/div/div[2]/div[2]/div[1]/div[1]/div[2]/span').text != "")
                print("Page is ready!")
                title = driver.find_element_by_xpath('/html/body/div[4]/div/div[1]/h1').text
                time = driver.find_element_by_xpath(
                    '/html/body/div[4]/div/div[2]/div[2]/div[1]/div[1]/div[2]/span').text
                return {'title': title, 'time': time}
            except TimeoutException:
                return {'message': 'Loading took too much time!'}, 500


@ns.route('/deleteVideo')
class Delete(Resource):
    def post(self):
        args = parser.parse_args()
        try:
            p = Popen("rm /var/www/html/record/{}.mp4".format(args['name']), shell=True, stdout=PIPE, stderr=PIPE)
            stdout, stderr = p.communicate()
            if len(stderr) == 0:
                return {'message': '{}.mp4 successfully deleted'.format(args['name'])}
            else:
                return {'message': str(stderr)}
        except Exception as e:
            return {'message': str(e)}, 500


@ns.route('/listVideos')
class List(Resource):
    def post(self):
        p = Popen("ls /var/www/html/record/", shell=True, stdout=PIPE, stderr=PIPE)
        stdout, stderr = p.communicate()
        clean = stdout.decode('utf-8').replace('\n', ' ')
        file_list = clean.split(' ')
        file_list.pop()
        return {'list': file_list}


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')
